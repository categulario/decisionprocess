Hello! @Alan has proposed to merge this. This is a **reversible** decision, which means that it will be affirmed once the "final comment period" of 10 days have passed, unless a team member places a "hold" on the decision (or cancels it).

| Team member | State |
|-------------|-------|
| @Alan | [merge](http://example.com/issue/1#comment=1) |
| @Barbara |  |
| @Grace |  |
| @Niklaus |  |
