use std::collections::HashMap;
use std::fmt;

use serde::{Serialize, Deserialize};
use chrono::{DateTime, Utc};

#[derive(Debug)]
pub enum Error {
    /// The first command that was given to this bot is not a valid one.
    /// Decision process must start with a resolution.
    InvalidFirstCommand,
}

use Error::*;

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
enum Reversibility {
    Reversible,
    Irreversible,
}

use Reversibility::*;

impl fmt::Display for Reversibility {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Reversible => write!(formatter, "a **reversible**"),
            Irreversible => writeln!(formatter, "an **irreversible**"),
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
enum Resolution {
    Hold,
    Custom(String),
}

use Resolution::*;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct UserStatus {
    name: String,
    issue_id: String,
    comment_id: String,
}

impl UserStatus {
    fn new(name: String, issue_id: String, comment_id: String) -> UserStatus {
        UserStatus {
            name, issue_id, comment_id,
        }
    }
}

impl fmt::Display for UserStatus {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}

pub trait LinkRenderer {
    fn render_link(&self, data: &UserStatus) -> String;
}

#[derive(Debug, Serialize, Deserialize)]
pub struct State {
    initiator: String,
    team_members: Vec<String>,
    period_start: DateTime<Utc>,
    original_period_start: DateTime<Utc>,
    current_statuses: HashMap<String, UserStatus>,
    status_history: HashMap<String, Vec<UserStatus>>,
    reversibility: Reversibility,
    resolution: Resolution,
}

impl State {
    /// Renders the current state to the form it will have when seen as a
    /// comment in the github issue/PR
    pub fn render(&self, renderer: &impl LinkRenderer) -> String {
        let initiator = &self.initiator;
        let reversibility = self.reversibility.to_string();
        let comment = format!("Hello! {initiator} has proposed to merge this. This is {reversibility} decision, which means that it will be affirmed once the \"final comment period\" of 10 days have passed, unless a team member places a \"hold\" on the decision (or cancels it).\n\n");

        let mut table = String::from(if self.status_history.is_empty() {
            "| Team member | State |\n\
             |-------------|-------|\n"
        } else {
            "| Team member | History | State |\n\
             |-------------|---------|-------|\n"
        });

        for member in self.team_members.iter() {
            let current_status = self.current_statuses.get(member).map(|status| {
                let link = renderer.render_link(status);

                format!("[{status}]({link})")
            }).unwrap_or_else(|| "".into());

            if self.status_history.is_empty() {
                table.push_str(&format!("| {member} | {current_status} |\n"));
            } else {
                let status_history = self.status_history.get(member).map(|statuses| {
                    statuses.iter().map(|status| {
                        let link = renderer.render_link(status);

                        format!("[{status}]({link})")
                    }).collect::<Vec<_>>().join(" ")
                }).unwrap_or_else(|| "".into());

                table.push_str(&format!("| {member} | {status_history} | {current_status} |\n"));
            }
        }

        comment + &table
    }
}

/// A command as parsed and received from calling the bot with some arguments,
/// like `@rustbot merge`
pub struct Command {
    user: String,
    disposition: Resolution,
    reversibility: Reversibility,
    issue_id: String,
    comment_id: String,
}

impl Command {
    #[cfg(test)]
    fn merge(user: String, issue_id: String, comment_id: String) -> Self {
        Self {
            user, issue_id, comment_id,
            disposition: Custom("merge".to_owned()),
            reversibility: Reversibility::Reversible,
        }
    }

    #[cfg(test)]
    fn hold(user: String, issue_id: String, comment_id: String) -> Self {
        Self {
            user, issue_id, comment_id,
            disposition: Hold,
            reversibility: Reversibility::Reversible,
        }
    }
}

pub struct Context {
    team_members: Vec<String>,
    now: DateTime<Utc>,
}

impl Context {
    pub fn new(team_members: Vec<String>, now: DateTime<Utc>) -> Context {
        Context {
            team_members, now,
        }
    }
}

/// Applies a command to the current state and returns the next state
pub fn handle(state: Option<State>, command: Command, context: Context) -> Result<State, Error> {
    let Command { user, issue_id, comment_id, disposition, reversibility } = command;

    if let Some(state) = state {
        let name = match disposition {
            Hold => "hold".into(),
            Custom(name) => name,
        };

        let mut current_statuses = state.current_statuses;
        let mut status_history = state.status_history;

        if let Some(entry) = current_statuses.get_mut(&user) {
            let past = status_history.entry(user).or_insert(Vec::new());

            past.push(entry.clone());

            *entry = UserStatus::new(name, issue_id, comment_id);
        } else {
            current_statuses.insert(user, UserStatus::new("hold".into(), issue_id, comment_id));
        }

        Ok(State {
            current_statuses,
            status_history,
            ..state
        })
    } else {
        // no state, this is the first call to the decision process
        match disposition {
            Hold => {
                Err(InvalidFirstCommand)
            }

            Custom(name) => {
                let mut statuses = HashMap::new();

                statuses.insert(user.clone(), UserStatus::new(name.clone(), issue_id, comment_id));

                let Context { team_members, now } = context;

                Ok(State {
                    initiator: user,
                    team_members,
                    period_start: now,
                    original_period_start: now,
                    current_statuses: statuses,
                    status_history: HashMap::new(),
                    reversibility,
                    resolution: Custom(name),
                })
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use chrono::Duration;
    use pretty_assertions::assert_eq;

    use super::*;

    struct TestRenderer {}

    impl LinkRenderer for TestRenderer {
        fn render_link(&self, data: &UserStatus) -> String {
            let issue_id = &data.issue_id;
            let comment_id = &data.comment_id;

            format!("http://example.com/issue/{issue_id}#comment={comment_id}")
        }
    }

    /// Example 1
    ///
    /// https://lang-team.rust-lang.org/decision_process/examples.html#reversible-decision-merging-a-proposal
    ///
    /// * From the starting point of there not being any state, someone proposes
    /// to merge a proposal
    /// * then barbara holds
    /// * 11 days pass
    /// * barbara says merge, it immediatly merges
    #[test]
    fn example_merging_proposal() {
        let team_members = vec![
            "@Alan".to_owned(), "@Barbara".to_owned(), "@Grace".to_owned(),
            "@Niklaus".to_owned(),
        ];
        let r = TestRenderer {};

        // alan proposes to merge
        let time1 = Utc::now();
        let command = Command::merge("@Alan".into(), "1".into(), "1".into());
        let state = handle(None, command, Context::new(team_members.clone(), time1)).unwrap();

        assert_eq!(state.period_start, time1);
        assert_eq!(state.original_period_start, time1);
        assert_eq!(state.current_statuses, vec![
            ("@Alan".into(), UserStatus::new("merge".into(), "1".into(), "1".into())),
        ].into_iter().collect());
        assert_eq!(state.status_history, HashMap::new());
        assert_eq!(state.reversibility, Reversibility::Reversible);
        assert_eq!(state.resolution, Custom("merge".into()));
        assert_eq!(state.render(&r), include_str!("../res/01_merging_proposal__1.md"));

        // barbara holds
        let time2 = Utc::now();
        let command = Command::hold("@Barbara".into(), "1".into(), "2".into());
        let state = handle(Some(state), command, Context::new(team_members.clone(), time2)).unwrap();

        assert_eq!(state.period_start, time1);
        assert_eq!(state.original_period_start, time1);
        assert_eq!(state.current_statuses, vec![
            ("@Alan".into(), UserStatus::new("merge".into(), "1".into(), "1".into())),
            ("@Barbara".into(), UserStatus::new("hold".into(), "1".into(), "2".into())),
        ].into_iter().collect());
        assert_eq!(state.status_history, HashMap::new());
        assert_eq!(state.reversibility, Reversibility::Reversible);
        assert_eq!(state.resolution, Custom("merge".into()));
        assert_eq!(state.render(&r), include_str!("../res/01_merging_proposal__2.md"));

        // 11 days pass
        let time3 = time2 + Duration::days(11);

        // Barbara says merge, it immediatly merges
        let command = Command::merge("@Barbara".into(), "1".into(), "3".into());
        let state = handle(Some(state), command, Context::new(team_members, time3)).unwrap();

        assert_eq!(state.period_start, time1);
        assert_eq!(state.original_period_start, time1);
        assert_eq!(state.current_statuses, vec![
            ("@Alan".into(), UserStatus::new("merge".into(), "1".into(), "1".into())),
            ("@Barbara".into(), UserStatus::new("merge".into(), "1".into(), "3".into())),
        ].into_iter().collect());
        assert_eq!(state.status_history, vec![
            ("@Barbara".into(), vec![UserStatus::new("hold".into(), "1".into(), "2".into())]),
        ].into_iter().collect());
        assert_eq!(state.reversibility, Reversibility::Reversible);
        assert_eq!(state.resolution, Custom("merge".into()));
        assert_eq!(state.render(&r), include_str!("../res/01_merging_proposal__3.md"));
    }
}
